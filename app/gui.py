from trainerbase.gui import add_codeinjection_to_gui, simple_trainerbase_menu

from injections import (
    infinite_hp,
    infinite_ammo_in_clip,
    infinite_money_on_use,
    instant_bullet_time,
)


@simple_trainerbase_menu("Call of Juarez: Bound in Blood", 400, 200)
def run_menu():
    add_codeinjection_to_gui(infinite_hp, "Infinite HP", "F1")
    add_codeinjection_to_gui(infinite_ammo_in_clip, "Infinite Ammo In Clip", "F2")
    add_codeinjection_to_gui(instant_bullet_time, "Instant Bullet Time", "F3")
    add_codeinjection_to_gui(infinite_money_on_use, "Infinite Money On Use", "F4")
