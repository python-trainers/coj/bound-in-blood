from types import SimpleNamespace

from pymem import process
from trainerbase.memory import pm


coj2_x86 = SimpleNamespace(
    dll=process.module_from_name(pm.process_handle, "CoJ2_x86.dll").lpBaseOfDll
)
