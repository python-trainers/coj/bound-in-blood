from os import _exit as force_exit
from gui import run_menu


def on_initialized():
    pass


def main():
    run_menu(on_initialized=on_initialized)
    force_exit(0)


if __name__ == "__main__":
    main()
