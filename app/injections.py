from trainerbase.codeinjection import AllocatingCodeInjection
from memory import coj2_x86


infinite_hp = AllocatingCodeInjection(
    coj2_x86.dll + 0x4C8026,
    """
        fsub dword [eax + 0x110]
        mov dword [eax + 0x110], 6666.0
    """,
    original_code_length=6,
)


infinite_ammo_in_clip = AllocatingCodeInjection(
    coj2_x86.dll + 0x198938,
    """
        mov dword [ecx + 0x610], 12
        mov eax, [ecx + 0x610]
    """,
    original_code_length=6,
)


instant_bullet_time = AllocatingCodeInjection(
    coj2_x86.dll + 0x489B2B,
    """
        cmp dword [ecx + 0x20], 1.0
        jnl return
        mov dword [ecx + 0x20], 2.0
        return:
        fcomp dword [ecx + 0x20]
        fnstsw ax
        test ah, 0x44
    """,
    original_code_length=8,
)


infinite_money_on_use = AllocatingCodeInjection(
    coj2_x86.dll + 0x4E5AA8,
    """
        mov dword [esi + 0x530], 99999
    """,
    original_code_length=6,
)
